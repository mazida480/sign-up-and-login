import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';

const routes: Routes = 
[
  {path:'signup', component:SignupComponent, title:'Sign-up'},
  {path:'', redirectTo:'/signup',pathMatch:'full', title:'Sign-up'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
