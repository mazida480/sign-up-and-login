import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit
{
  newUser:User = new User("Abdul", "Mazid", "mazida480@gmail.com", "Popo@3793")

  userList:User[] = []
  currentUser:number = -1
  ngOnInit()
  {
    console.log(JSON.stringify(this.newUser))
  }

  addUser()
  {
    if(this.userList.includes(this.newUser))
    {
      alert("User already exist.")
    }
    else
    {
      this.userList.push(this.newUser)
      console.log(this.userList)
    }
  }
}
